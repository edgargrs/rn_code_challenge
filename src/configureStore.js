import { applyMiddleware, createStore, compose } from 'redux';
import { persistReducer, persistStore } from 'redux-persist';
import createSagaMiddleware from 'redux-saga';
import { REDUX_PERSIST, DebugConfig } from 'config';
import Reactotron from 'config/ReactotronConfig'
import appReducer from './reducers';
import appSaga from './sagas';

const sagaMiddleware = createSagaMiddleware();

const configureStore = () => {
    const middlewares = [sagaMiddleware];
    const enhancers = DebugConfig.useReactotron ? [applyMiddleware(...middlewares), Reactotron.createEnhancer()] : [applyMiddleware(...middlewares)];
    const persistedReducer = persistReducer(REDUX_PERSIST, appReducer);

    const store = createStore(
        persistedReducer,
        appReducer(undefined, {}),
        compose(...enhancers)
    );

    const persistor = persistStore(store);

    sagaMiddleware.run(appSaga);

    return { store, persistor };
}

export default configureStore;