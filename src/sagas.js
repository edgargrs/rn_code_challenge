import { takeLatest, put, fork, putResolve } from 'redux-saga/effects';
import { STARTUP } from 'features/app/constants';
import splashSaga from 'features/splash/sagas';
import categoriesSaga from 'features/categories/sagas';
import favoritesSaga from 'features/favorites/sagas';
import { fetchCategories } from 'features/categories/actions';

function* watchAppStart(action) {
  // APP START
  yield put(fetchCategories());
}

export default function* appSaga() {
  yield takeLatest(STARTUP, watchAppStart);
  yield fork(splashSaga);
  yield fork(categoriesSaga);
  yield fork(favoritesSaga);
}
