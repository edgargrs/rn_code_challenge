import React, { useState, useEffect } from 'react';
import {
  View,
  FlatList,
  TouchableOpacity,
} from 'react-native';
import styles from './styles'
import { Searchbar, Card, ActivityIndicator, Title } from 'react-native-paper';
import CategoriesList from 'features/categories/containers';
import api from 'services/api';
import {size} from 'lodash';
import {
  navigateToAnimeDetail,
  navigateToMangaDetail
} from 'navigation/actions';
import colors from 'components/config/colors';

function SearchResults({results}){
  return (
    <FlatList
        data={results.data}
        numColumns={2}
        ListHeaderComponent={<Title>{"Search results"}</Title>}
        renderItem={({ item }) => (
          <View style={styles.item}>
            <Card>
              <TouchableOpacity onPress={() => item.type == "anime" ? navigateToAnimeDetail(item) : navigateToMangaDetail(item) }>
                <Card.Cover source={{ uri: item.attributes.posterImage.small }} />
                <Card.Title title={item.attributes.titles.en || item.attributes.canonicalTitle} />
              </TouchableOpacity>
            </Card>
          </View>
        )}
        keyExtractor={(item) => item.id}
      />
  );
}


function Manga(){
  const [typingTimeout, setTypingTimeout] = useState(0);
  const [isTyping, setIsTyping] = useState(false);
  const [searchQuery, setSearchQuery] = useState('');
  const [searchResults, setSearchResults] = useState({});

  function handleChangeText(text){
    if(typingTimeout){
        if(!isTyping){
            setIsTyping(true);
        }
        clearTimeout(typingTimeout);
    }
    setSearchQuery(text);
    if(text.length == 0){
      setIsTyping(false);
      setSearchResults({});
    }
    setTypingTimeout(setTimeout(function () {
        setIsTyping(false);
    }, 1500));
  }

  useEffect(() => {
    async function searchAnimes(){
      if(searchQuery.length > 0){
        const {data} = await api.getSearch('manga', searchQuery);
        setSearchResults(data);
      }
    }
    searchAnimes();
  }, [!isTyping]);

  return(
    <View style={styles.container}>
      <View style={styles.searchContainer}>
        <Searchbar
          placeholder="Search"
          onChangeText={text => handleChangeText(text)}
          value={searchQuery}
        />
      </View>
      <View style={styles.content}>
        {isTyping && <ActivityIndicator animating={true} color={colors.secondary} />}
        {searchQuery.length == 0 && <CategoriesList type={"manga"} />}
        {size(searchResults) > 0 && !isTyping && <SearchResults results={searchResults}/>}
      </View>
    </View>
  );
}


export default Manga;
