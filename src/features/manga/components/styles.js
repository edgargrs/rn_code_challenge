import { StyleSheet, Platform } from 'react-native';
import colors from 'components/config/colors';

export default StyleSheet.create({
  container: {
    flex: 1,
  },
  searchContainer: {
    backgroundColor: colors.primary_second,
    paddingHorizontal: 12,
    paddingVertical: 16
  },
  content: {
    flex: 1,
    backgroundColor: '#F6F6F7',
    padding: 16,
  },
  item:{
    marginVertical: 8,
    marginHorizontal: 8,
    flex: 1,
  }
});


