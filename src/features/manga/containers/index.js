import { connect } from 'react-redux';
import Component from 'features/manga/components';
import { bindActionCreators } from 'redux';
import {
  getCategories
} from 'features/categories/selectors';

const mapStateToProps = state => ({
  categories: getCategories(state)
});

const mapDispatchToProps = dispatch => bindActionCreators({ 

  }, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Component);