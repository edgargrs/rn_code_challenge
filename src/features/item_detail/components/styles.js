import { StyleSheet, Platform } from 'react-native';
import colors from 'components/config/colors';

export default StyleSheet.create({
  container: {
    flex: 1,
  },
  backgroundImage: {
    width: '100%',
    flex: 1,
    resizeMode: 'cover',
    justifyContent: 'center',
    alignItems: 'center'
  },
  titleImage: {
    color: colors.icon,
    textAlign: 'center',
    paddingHorizontal: 20
  },
  content: {
    width: '100%',
    backgroundColor: '#F6F6F7',
    marginBottom: 50,
  },
  contentCont: {
    width:'100%',
    flexDirection: 'row',
    padding: 8,
  },
  imgPosterCont: {
    flex: 1,
    width: '100%',
    padding: 8
  },
  mainDetailsCont: {
    flex: 1.5,
    width: '100%',
    padding: 8
  },
  cardMainDetails: {
    flex: 1,
    paddingHorizontal: 16,
    paddingVertical: 4
  },
  detailsCont: {
    paddingHorizontal: 16,
    marginBottom: 16,
  },
  cardDetailsTitle:{
    paddingHorizontal: 16,
    paddingTop: 8
  },
  cardDetails:{
    paddingHorizontal: 16,
    paddingVertical: 4,
    flexDirection: 'row',
  },
  cardColumn: {
    width: '50%',
  },
  fab: {
    position: 'absolute',
    margin: 16,
    right: 0,
    bottom: 0,
    backgroundColor: "red"
  },
});


