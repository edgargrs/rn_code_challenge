import React, { useState, useEffect } from 'react';
import {
  View,
  ImageBackground,
  Linking,
  ScrollView,
  FlatList,
  Share
} from 'react-native';
import { get, isNull, isEmpty } from 'lodash';
import ParallaxScrollView from 'react-native-parallax-scroll-view';
import colors from 'components/config/colors';
import {
  Title,
  ActivityIndicator,
  Appbar,
  Headline,
  Card,
  Caption,
  Paragraph,
  FAB,
  List,
  Button,
  Dialog,
  Portal
} from 'react-native-paper';
import { navigateBack } from 'navigation/actions';
import styles from './styles.js';
import axios from 'axios';

function ListChapters({url, type}){
  const [characters, setChapters] = useState(null);
  const [isLoading, setLoading] = useState(true);

  const getChapters = () => axios.get(url);




  useEffect(() => {
    getChapters().then(({data}) => {setChapters(data); setLoading(false);})
  }, []);

  if(isLoading){
    return(
      <View style={{ minHeight: 150 }}>
        <ActivityIndicator animating={true} color={colors.secondary} />
      </View>
    );
  }

  return(
    <View style={styles.detailsCont}>
          <Card>
          <Card.Content>
            <Title>{type == "anime" ? "Episode" : "Chapter"}</Title>
            <FlatList
              data={characters.data}
              renderItem={({ item }) => (
                <List.Item
                  title={((!isNull(item.attributes.canonicalTitle) && !isEmpty(item.attributes.canonicalTitle)) ? item.attributes.canonicalTitle : "" ) + (type == "anime" ? " Episode " : " Chapters ") + item.attributes.number}
                  description={item.attributes.description}
                  left={() => <List.Icon icon="movie" />}
                />
              )}
              keyExtractor={(item) => item.id}
            />
          </Card.Content>
      </Card>
    </View>
  );
}

function ItemDetail({route, addFavoriteFlow}){
  const { item } = route.params
  const primary_color = item.type == "anime" ? colors.primary : colors.primary_second;
  const coverImage = item.attributes.coverImage != null ? item.attributes.coverImage.original : null
  const title = item.attributes.titles.en || item.attributes.canonicalTitle;
  const description = item.attributes.synopsis|| item.attributes.description;
  const [favoriteVisible, setFavoriteVisible] = useState(false);
  const showFavoriteDialog = () => setFavoriteVisible(true);
  const hideFavoriteDialog = () => setFavoriteVisible(false);
  
  const renderAnimeMainInfo = () => {
    const {
      startDate = "2014-10-22",
      endDate = "2014-10-22",
      episodeCount = "0",
      showType = "",
    } = item.attributes;
    return (
      <>
        <Title numberOfLines={1}>{title}</Title>
        <Caption>Type: </Caption>
        <Paragraph>{showType}</Paragraph>
        <Caption>Number of Episodes: </Caption>
        <Paragraph>{episodeCount}</Paragraph>
        <Caption>Year: </Caption>
        <Paragraph>{startDate+ " - " + (isNull(endDate) ? "" : endDate)}</Paragraph>
      </>
    );
  }

  const renderAnimDetailInfo = () => {
    const {
      averageRating = "0",
      popularityRank = "0",
      ageRating = "0",
      episodeLength = "0",
      totalLength = "0",
      status = "",
    } = item.attributes;
    return (
      <>
        <View style={styles.cardDetails}>
          <View style={styles.cardColumn}>
            <Caption>Average Rating: </Caption>
            <Paragraph>{averageRating}</Paragraph>
            <Caption>Episode Duration </Caption>
            <Paragraph>{episodeLength}</Paragraph>
            <Caption>Total Duration </Caption>
            <Paragraph>{totalLength}</Paragraph>
          </View>
          <View style={styles.cardColumn}>
            <Caption>Age Rating: </Caption>
            <Paragraph>{ageRating}</Paragraph>
            <Caption>Popularity Rank: </Caption>
            <Paragraph>{popularityRank}</Paragraph>
            <Caption>Status: </Caption>
            <Paragraph>{status}</Paragraph>
          </View>
        </View>
      </>
    );
  }

  const renderMangaMainInfo = () => {
    const {
      startDate = "",
      endDate = "",
      popularityRank = "",
      mangaType = "",
    } = item.attributes;
    return(
      <>
        <Title numberOfLines={1}>{title}</Title>
        <Caption>Manga Type: </Caption>
        <Paragraph>{mangaType}</Paragraph>
        <Caption>Popularity Rank: </Caption>
        <Paragraph>{popularityRank}</Paragraph>
        <Caption>Year: </Caption>
        <Paragraph>{startDate + " - " + (isNull(endDate) ? "" : endDate)}</Paragraph>
      </>
    );
  }

  const renderMangaDetailInfo = () => {
    const {
      averageRating,
      popularityRank,
      ratingRank,
      status,
      chapterCount,
      volumeCount,
    } = item.attributes;
    return(
      <>
        <View style={styles.cardDetails}>
          <View style={styles.cardColumn}>
            <Caption>Number of chapters: </Caption>
            <Paragraph>{chapterCount}</Paragraph>
            <Caption>Number of volumes </Caption>
            <Paragraph>{volumeCount}</Paragraph>
            <Caption>Average Rating </Caption>
            <Paragraph>{averageRating}</Paragraph>
          </View>
          <View style={styles.cardColumn}>
            <Caption>Rating Rank: </Caption>
            <Paragraph>{ratingRank}</Paragraph>
            <Caption>Popularity Rank: </Caption>
            <Paragraph>{popularityRank}</Paragraph>
            <Caption>Status: </Caption>
            <Paragraph>{status}</Paragraph>
          </View>
        </View>
      </>
    );
  }


  function Content(){
    return (
      <>
        <View style={styles.contentCont}>
          <View style={styles.imgPosterCont}>
            <Card>
                <Card.Cover source={{ uri: item.attributes.posterImage.small }} />
            </Card>
          </View>
          <View style={styles.mainDetailsCont}>
            <Card style={styles.cardMainDetails}>
              {item.type == "anime" ? renderAnimeMainInfo() : renderMangaMainInfo()}
            </Card>
          </View>
        </View>
        <View style={styles.detailsCont}>
          <Card>
            <Title style={styles.cardDetailsTitle} numberOfLines={1}>{"Details"}</Title>
            {item.type == "anime" ?  renderAnimDetailInfo() : renderMangaDetailInfo() }
          </Card>
        </View>
        {!isNull(description) && !isEmpty(description) &&
          <View style={styles.detailsCont}>
            <Card>
            <Card.Content>
              <Title>Description</Title>
              <Paragraph>{description}</Paragraph>
            </Card.Content>
            </Card>
          </View>
        }
        <ListChapters type={item.type} url={item.type == "anime" ? item.relationships.episodes.links.related : item.relationships.chapters.links.related} />
      </>
    );
  }

  const share = async() => {
    await Share.share({
      title: title,
      message: '¡Hey! check this ' + item.type + ",  " + title + ", " + description
    });
  }

  const {
    youtubeVideoId = null,
  } = item.attributes;
  return(
    <>
      <Appbar.Header style={{ backgroundColor: primary_color }}>
        <Appbar.BackAction onPress={navigateBack} />
        <Appbar.Content title={title} />
          <Appbar.Action icon={"heart"} onPress={() => showFavoriteDialog()} />
          <Appbar.Action icon={"share-variant"} onPress={share} />
      </Appbar.Header>
      {!isNull(coverImage) &&
        <ParallaxScrollView
          backgroundColor={primary_color}
          backgroundScrollSpeed={30}
          stickyHeaderHeight={55}
          parallaxHeaderHeight={280}
          renderForeground={() => (
            <ImageBackground source={{ uri: coverImage}} style={styles.backgroundImage} imageStyle={{ opacity: 0.65}}>
              <Headline style={styles.titleImage}>{title}</Headline>
              {item.type == "anime" && youtubeVideoId != null &&
                <FAB
                  style={styles.fab}
                  small
                  icon="youtube"
                  onPress={() => Linking.openURL("https://www.youtube.com/watch?v="+youtubeVideoId)}
                />
              }
            </ImageBackground>
          )}>
          <View style={styles.content}>
            <Content />
          </View>
        </ParallaxScrollView>
      }
      {isNull(coverImage) && 
        <View style={styles.content}>
          <ScrollView>
            <Content />
          </ScrollView>
        </View>
      }
      <Portal>
        <Dialog visible={favoriteVisible} onDismiss={hideFavoriteDialog}>
          <Dialog.Title>Favorites</Dialog.Title>
          <Dialog.Content>
            <Paragraph>Do you want to add to your favorites list?</Paragraph>
          </Dialog.Content>
          <Dialog.Actions>
            <Button onPress={hideFavoriteDialog}>NO</Button>
            <Button onPress={()=> {
              addFavoriteFlow(item);
              hideFavoriteDialog();
            }}>YES</Button>
          </Dialog.Actions>
        </Dialog>
      </Portal>

    </>
  );
}


export default ItemDetail;
