import { connect } from 'react-redux';
import Component from 'features/item_detail/components';
import { bindActionCreators } from 'redux';
import {
  addFavoriteFlow
} from 'features/favorites/actions';

const mapStateToProps = state => ({

});

const mapDispatchToProps = dispatch => bindActionCreators({ 
  addFavoriteFlow
}, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Component);