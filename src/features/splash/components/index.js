import React from 'react';
import {
  View,
  Text,
  StatusBar
} from 'react-native'
import styles from './styles';
import {
  ActivityIndicator,
} from 'react-native-paper';
import colors from 'components/config/colors';

class Splash extends React.Component {

  constructor(props){
    super(props);
  }

  handleWelcome = () => {
    setTimeout(() => {
      this.props.handleWelcome()
    }, 1000);
  }

  componentDidMount(){
    this.handleWelcome();
  }

  render() {
    return (
      <>
        <StatusBar backgroundColor="#fff"/>
        <View style={styles.container}>
          <ActivityIndicator animating={true} color={colors.secondary} />
        </View>
      </>
    )
  }
}

export default Splash;