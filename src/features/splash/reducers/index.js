import {
  SET_APP_VERSION,
} from '../constants';
import { handleActions } from 'redux-actions';

const initialState = {
  app_version: true,
};

const splashReducer = handleActions({
  [SET_APP_VERSION]: (state, action) => ({
    ...state,
    app_version: action.payload
  }),
},initialState);

export default splashReducer;