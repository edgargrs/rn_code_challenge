import { createAction } from 'redux-actions';
import {
  HANDLE_WELCOME,
  SET_APP_VERSION
} from '../constants';

export const handleWelcome = createAction(HANDLE_WELCOME);

export const setAppVersion = createAction(SET_APP_VERSION);