import { createSelector } from 'reselect';
import { get } from 'lodash';

export const getSplashState = state => get(state, 'splash');

export const getAppVersion = createSelector([getSplashState], splash => get(splash, 'app_version'));