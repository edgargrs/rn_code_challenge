import { takeLatest, call, put } from 'redux-saga/effects';
import {
  HANDLE_WELCOME,
} from '../constants';
import {
  navigateToHome
} from 'navigation/actions';

function* handleWelcome() {
  navigateToHome();
}

export default function*() {
  yield takeLatest(HANDLE_WELCOME, handleWelcome);
}
