import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Splash from 'features/splash/components';
import {
  handleWelcome
} from 'features/splash/actions';

const mapDispatchToProps = dispatch => bindActionCreators({ handleWelcome }, dispatch);


export default connect(
  null,
  mapDispatchToProps
)(Splash);