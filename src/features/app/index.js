import 'react-native-gesture-handler';
import React from "react";
import { Provider } from "react-redux";
import { PersistGate } from "redux-persist/integration/react";
import configureStore from "configureStore";
import AppContainer from 'features/app/containers';

const { store, persistor } = configureStore();

export default App = () => {

  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <AppContainer />
      </PersistGate>
    </Provider>
  );
}
