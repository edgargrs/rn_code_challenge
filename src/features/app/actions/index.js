import { STARTUP } from "../constants";
import { createAction } from 'redux-actions';

export const startup = createAction(STARTUP);