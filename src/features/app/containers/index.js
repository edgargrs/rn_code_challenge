import { connect } from "react-redux";
import { bindActionCreators } from 'redux';
import Component from "features/app/components";
import { startup } from "features/app/actions";

const mapDispatchToProps = dispatch => bindActionCreators({
  startup,
}, dispatch);

export default connect(
  null,
  mapDispatchToProps
)(Component);
