import React, { Component } from 'react';
import Navigation from "navigation/containers";
import { Provider } from 'react-native-paper';
class AppComponent extends Component {

  componentDidMount() {
    this.props.startup();
  }

  render() {
    return(
      <Provider>
        <Navigation />
      </Provider>
    );
  }
}
export default AppComponent;
