import {
  SET_CATEGORIES,
  SET_LOADING,
  CLEAR_CATEGORIES
} from '../constants';
import { handleActions } from 'redux-actions';

const initialState = {
  isLoading: false,
  model: {},
};

const categoriesReducer = handleActions({
  [SET_CATEGORIES]: (state, {payload}) => ({
    ...state,
    model: payload
  }),
  [SET_LOADING]: (state, {payload}) => ({
    ...state,
    isLoading: payload
  }),
  [CLEAR_CATEGORIES]: (state, {payload}) =>
    initialState,
},initialState);

export default categoriesReducer;