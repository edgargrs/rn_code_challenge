export const SET_LOADING = 'categories/SET_LOADING';
export const FETCH_CATEGORIES = 'categories/FETCH_CATEGORIES';
export const SET_CATEGORIES = 'categories/SET_CATEGORIES';
export const CLEAR_CATEGORIES = 'categories/CLEAR_CATEGORIES';