import { createSelector } from 'reselect';
import { get, map  } from 'lodash';

export const getCategoriesState = state => get(state, 'categories');

export const getIsCategoriesLoading = createSelector([getCategoriesState], categories => get(categories, 'isLoading'));

export const getCategories = createSelector([getCategoriesState], categories =>
  get(categories, ['model', 'data'])
);