import { createAction } from 'redux-actions';
import {
  SET_LOADING,
  SET_CATEGORIES,
  FETCH_CATEGORIES
} from '../constants';

export const setLoading = createAction(SET_LOADING);

export const setCategories = createAction(SET_CATEGORIES);

export const fetchCategories = createAction(FETCH_CATEGORIES);