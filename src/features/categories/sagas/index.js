import { takeLatest, call, put } from 'redux-saga/effects';
import {
  FETCH_CATEGORIES,
} from '../constants';
import {
  setCategories,
  setLoading
} from 'features/categories/actions';
import api from 'services/api';
import Reactotron from 'reactotron-react-native';

export function* do_request(endpoint, payload){
  try{
    const response = yield call(endpoint, payload)
    return response;
  } catch(error){
    return false;
  }
}

function* fetchCategories() {
  yield put(setLoading(true));
  const response = yield call(do_request, api.getCategories);
  const { data }= response;
  yield put(setCategories(data));
  yield put(setLoading(false));
}

export default function*() {
  yield takeLatest(FETCH_CATEGORIES, fetchCategories);
}
