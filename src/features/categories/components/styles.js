import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  categoryItem:{
    marginVertical: 8
  },
  categoryItemList:{
    width: '100%',
    marginVertical: 8
  },
  item:{
    width:150,
    marginRight: 16
  },
  favoriteContainer:{
    position: 'absolute',
    top: 4,
    right: 4
  }
});