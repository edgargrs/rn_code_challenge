import React, { useState, useEffect } from 'react';
import {
  View,
  FlatList,
  TouchableOpacity
} from 'react-native';
import styles from './styles'
import { Title, Subheading, ActivityIndicator, Card } from 'react-native-paper';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import colors from 'components/config/colors';
import axios from 'axios';
import {
  navigateToAnimeDetail,
  navigateToMangaDetail
} from 'navigation/actions';

function CategoryItemList({
  id,
  type,
  url,
}){
  const [animes, setAnimes] = useState(null);
  const [isLoading, setLoading] = useState(true);
  const getItems = () => axios.get(url);

  useEffect(() => {
    getItems().then(({data}) => {setAnimes(data); setLoading(false);})
  }, []);

  if(isLoading){
    return(
      <View style={{ minHeight: 150 }}>
        <ActivityIndicator animating={true} color={colors.secondary} />
      </View>
    );
  }

  function handlePressItem(item){
    if(type == "anime"){
      navigateToAnimeDetail(item);
    }else{
      navigateToMangaDetail(item);
    };
  };

  return(
    <View style={styles.categoryItemList}>
      <FlatList
        data={animes.data}
        horizontal
        renderItem={({ item }) => (
          <View style={styles.item}>
            <Card>
              <TouchableOpacity onPress={() => handlePressItem(item)}>
                <Card.Cover source={{ uri: item.attributes.posterImage.small }} />
                <Card.Title title={item.attributes.titles.en || item.attributes.canonicalTitle} />
              </TouchableOpacity>
            </Card>
          </View>
        )}
        keyExtractor={(item) => item.id}
      />
    </View>
  );
}


function CategoryList({
  categories,
  type
}){
  return (
    <FlatList
      data={categories}
      ListHeaderComponent={<Title>{type.toUpperCase()}</Title>}
      renderItem={({ item }) => (
        <View style={styles.categoryItem}>
          <Subheading>{item.attributes.title}</Subheading>
          <CategoryItemList
            id={item.item}
            type={type}
            url={item.relationships[type].links.related}
          />
        </View>
      )}
      keyExtractor={(item) => item.id}
    />
  );
}

export default CategoryList;