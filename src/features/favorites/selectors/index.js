import { createSelector } from 'reselect';
import { get, filter  } from 'lodash';

export const getFavoritesState = state => get(state, 'favorites');

export const getIsFavoritesLoading = createSelector([getFavoritesState], favorites => get(favorites, 'isLoading'));

export const getFavorites = createSelector([getFavoritesState], favorites =>
  get(favorites, ['model'], [])
);

export const getAnimeFavorites = createSelector([getFavorites], favorites =>
  filter(favorites, favorite => favorite.type == "anime")
);

export const getMangaFavorites = createSelector([getFavorites], favorites =>
  filter(favorites, favorite => favorite.type == "manga")
);