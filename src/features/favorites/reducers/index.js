import {
  ADD_FAVORITE,
  SET_LOADING,
  DELETE_FAVORITE,
  CLEAR_FAVORITE
} from '../constants';
import { handleActions } from 'redux-actions';

const initialState = {
  isLoading: false,
  model: [],
};

const favoritesReducer = handleActions({
  [ADD_FAVORITE]: (state, {payload}) => ({
    ...state,
    model: [...state.model, payload]
  }),
  [DELETE_FAVORITE]: (state, {payload}) => ({
    ...state,
    model: [...state.model.filter(favorite => favorite !== payload)]
  }),
  [SET_LOADING]: (state, {payload}) => ({
    ...state,
    isLoading: payload
  }),
  [CLEAR_FAVORITE]: (state, {payload}) =>
    initialState,
},initialState);

export default favoritesReducer;