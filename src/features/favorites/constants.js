export const SET_LOADING = 'favorites/SET_LOADING';
export const ADD_FAVORITE = 'favorites/ADD_FAVORITE';
export const ADD_FAVORITE_FLOW = 'favorites/ADD_FAVORITE_FLOW';
export const DELETE_FAVORITE = 'favorites/DELETE_FAVORITE';
export const CLEAR_FAVORITE = 'favorites/CLEAR_FAVORITE';