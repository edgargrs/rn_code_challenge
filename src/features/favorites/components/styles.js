import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  categoryItem:{
    marginVertical: 8
  },
  categoryItemList:{
    width: '100%',
    marginVertical: 8,
    marginHorizontal: 12,
  },
  item:{
    width:150,
    marginHorizontal: 12
  },
  favoriteContainer:{
    position: 'absolute',
    top: 4,
    right: 4
  }
});