import React, { useState } from 'react';
import {
  View,
  FlatList,
  TouchableOpacity
} from 'react-native';
import {
  Title,
  Appbar,
  Card,
  Paragraph,
  Button,
  Dialog,
  Portal
} from 'react-native-paper';
import colors from 'components/config/colors';
import { size } from 'lodash';
import styles from './styles.js';

function Favorites({
  animeFavorites,
  mangaFavorites,
  deleteFavorite
}){
  const [deleteVisible, setDeleteVisible] = useState(false);
  const [selectedItem, setSelectedItem] = useState(null);
  const showDeleteDialog = () => setDeleteVisible(true);
  const hideDeleteDialog = () => setDeleteVisible(false);



  const handlePressItem = (item) => {
    setSelectedItem(item);
    showDeleteDialog();
  }

  const handleDeleteItem = () => {
    deleteFavorite(selectedItem);
    hideDeleteDialog();
  }

  return(
    <>
      <Appbar.Header style={{ backgroundColor: colors.divider }}>
        <Appbar.Content title={"FAVORITES"} />
      </Appbar.Header>
      {size(animeFavorites) > 0 &&
        <View style={styles.categoryItemList}>
          <Title>{"ANIME"}</Title>
          <FlatList
            horizontal
            data={animeFavorites}
            renderItem={({ item }) => (
              <View style={styles.item}>
                <Card>
                  <TouchableOpacity onPress={() => handlePressItem(item)}>
                    <Card.Cover source={{ uri: item.attributes.posterImage.small }} />
                    <Card.Title title={item.attributes.titles.en || item.attributes.canonicalTitle} />
                  </TouchableOpacity>
                </Card>
              </View>
            )}
            keyExtractor={(item) => item.id}
          />
        </View>
      }
      {size(mangaFavorites) > 0 &&
        <View style={styles.categoryItemList}>
          <Title>{"MANGA"}</Title>
          <FlatList
            horizontal
            data={mangaFavorites}
            renderItem={({ item }) => (
              <View style={styles.item}>
                <Card>
                  <TouchableOpacity onPress={() => handlePressItem(item)}>
                    <Card.Cover source={{ uri: item.attributes.posterImage.small }} />
                    <Card.Title title={item.attributes.titles.en || item.attributes.canonicalTitle} />
                  </TouchableOpacity>
                </Card>
              </View>
            )}
            keyExtractor={(item) => item.id}
          />
        </View>
      }
      <Portal>
        <Dialog visible={deleteVisible} onDismiss={hideDeleteDialog}>
          <Dialog.Title>Favorites</Dialog.Title>
          <Dialog.Content>
            <Paragraph>Do you want to deletef from your favorites list?</Paragraph>
          </Dialog.Content>
          <Dialog.Actions>
            <Button onPress={hideDeleteDialog}>NO</Button>
            <Button onPress={handleDeleteItem}>YES</Button>
          </Dialog.Actions>
        </Dialog>
      </Portal>
    </>
  );
}


export default Favorites;
