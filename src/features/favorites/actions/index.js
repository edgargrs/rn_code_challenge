import { createAction } from 'redux-actions';
import {
  SET_LOADING,
  ADD_FAVORITE,
  ADD_FAVORITE_FLOW,
  DELETE_FAVORITE,
  CLEAR_FAVORITE
} from '../constants';

export const setLoading = createAction(SET_LOADING);

export const addFavorite = createAction(ADD_FAVORITE);

export const addFavoriteFlow = createAction(ADD_FAVORITE_FLOW);

export const deleteFavorite = createAction(DELETE_FAVORITE);

export const clearFavorites = createAction(CLEAR_FAVORITE);