import { takeLatest, put, select } from 'redux-saga/effects';
import {
  ADD_FAVORITE_FLOW,
} from '../constants';
import {
  setLoading
} from 'features/favorites/actions';
import {
  getFavorites
} from 'features/favorites/selectors';
import { addFavorite } from 'features/favorites/actions';
import { find } from 'lodash';

function* addFavoriteFlow({payload}) {
  yield put(setLoading(true));
  const favorites = yield select(getFavorites);
  const favorite = find(favorites, favorite => favorite.id == payload.id && favorite.type == payload.type);
  if(!favorite){
    yield put(addFavorite(payload));
  }
  yield put(setLoading(false));
  
}

export default function*() {
  yield takeLatest(ADD_FAVORITE_FLOW, addFavoriteFlow);
}
