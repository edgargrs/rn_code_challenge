import { connect } from 'react-redux';
import Component from 'features/favorites/components';
import { bindActionCreators } from 'redux';
import {
  getAnimeFavorites,
  getMangaFavorites
} from 'features/favorites/selectors';

import {
  deleteFavorite
} from 'features/favorites/actions';

const mapStateToProps = state => ({
  animeFavorites: getAnimeFavorites(state),
  mangaFavorites: getMangaFavorites(state)
});

const mapDispatchToProps = dispatch => bindActionCreators({ 
  deleteFavorite
}, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Component);