import React, { useEffect } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import RootNavigator from 'navigation/navigators';
import { navigationRef, isReadyRef } from 'navigation/actions';
import { BackHandler } from 'react-native';

function Navigation() {
  useEffect(() => {
    return () => {
      isReadyRef.current = false
    };
  }, []);

  useEffect(() => {
    const backAction = () => {
      // Here we need get current screen name
      return true;
    };
    const backHandler = BackHandler.addEventListener(
      "hardwareBackPress",
      backAction
    );
    return () => backHandler.remove();
  }, []);

  return (
    <NavigationContainer
      ref={navigationRef}
      onReady={() => {
        isReadyRef.current = true;
      }}
    >
      <RootNavigator />
    </NavigationContainer>
  );
}

export default Navigation;