import * as React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { Text, View } from "react-native";
import Home from 'navigation/navigators/home';
import * as screenNames from 'navigation/screen_names';
import Splash from 'features/splash/containers';

const Stack = createStackNavigator();

function RootNavigator() {

  return (
    <Stack.Navigator>
      <Stack.Screen name={screenNames.SPLASH} component={Splash} options={{ headerShown: false }}/>
      <Stack.Screen name={screenNames.HOME} component={Home} options={{ headerShown: false }} />
    </Stack.Navigator>
  );
}

export default RootNavigator;