import React, {useState} from 'react';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import { createStackNavigator } from '@react-navigation/stack';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {
  StatusBar,
} from 'react-native';
import Anime from 'features/anime/containers';
import Manga from 'features/manga/containers';
import ItemDetail from 'features/item_detail/containers';
import Favorites from 'features/favorites/containers';
import * as screenNames from 'navigation/screen_names';
import colors from 'components/config/colors';

const Tab = createMaterialBottomTabNavigator();
const Stack = createStackNavigator();

function AnimeNavigator(){
  return (
    <Stack.Navigator>
      <Stack.Screen name={screenNames.ANIME} component={Anime} options={{ headerShown: false }}/>
      <Stack.Screen name={screenNames.ANIME_DETAIL} component={ItemDetail} options={{ headerShown: false }} />
    </Stack.Navigator>
  );
}

function MangaNavigator(){
  return (
    <Stack.Navigator>
      <Stack.Screen name={screenNames.MANGA} component={Manga} options={{ headerShown: false }}/>
      <Stack.Screen name={screenNames.MANGA_DETAIL} component={ItemDetail} options={{ headerShown: false }} />
    </Stack.Navigator>
  );
}


function Home({navigation}) {
    const [currentTab, setCurrenTab] = useState(0);
    const statusbarColor = () => currentTab == 0 ? colors.dark_primary : (currentTab == 1 ? colors.dark_primary_second: colors.divider)
    return (
      <>
        <StatusBar hidden={false} backgroundColor={statusbarColor()} animated/>
        <Tab.Navigator
          initialRouteName={screenNames.TAB_ANIME}
          activeColor={colors.secondary}
          inactiveColor={colors.secondary_text}
          barStyle={{ backgroundColor: colors.white }}
        >
          <Tab.Screen name={screenNames.TAB_ANIME} component={AnimeNavigator}
            options={{
              tabBarLabel: 'Anime',
              tabBarIcon: ({ color }) => (
                <MaterialCommunityIcons name="television" color={color} size={26} />
              ),
            }}
            listeners={{ tabPress: e => setCurrenTab(0) }}
          />
          <Tab.Screen name={screenNames.TAB_MANGA} component={MangaNavigator}
            options={{
              tabBarLabel: 'Manga',
              tabBarIcon: ({ color }) => (
                <MaterialCommunityIcons name="book-open-page-variant" color={color} size={26} />
              ),
            }}
            listeners={{ tabPress: e => setCurrenTab(1) }}
          />
          <Tab.Screen name={screenNames.FAVORITES} component={Favorites}
            options={{
              tabBarLabel: 'Favorites',
              tabBarIcon: ({ color }) => (
                <MaterialCommunityIcons name="heart" color={color} size={26} />
              ),
            }}
            listeners={{ tabPress: e => setCurrenTab(2) }}
          />
        </Tab.Navigator>
      </>
    );
}

export default Home;