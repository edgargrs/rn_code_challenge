import * as React from 'react';
import * as screenNames from 'navigation/screen_names';


export const isReadyRef = React.createRef();

export const navigationRef = React.createRef();

export function navigate(name, params) {
  if (isReadyRef.current && navigationRef.current) {
    navigationRef.current.navigate(name, params);
  }
}

export function goBack() {
  if (isReadyRef.current && navigationRef.current) {
    navigationRef.current.goBack();
  }
}

export const navigateToHome = () => navigate(screenNames.HOME);

export const navigateToAnimeDetail = item => navigate(screenNames.ANIME_DETAIL, { item: item });

export const navigateToMangaDetail = item => navigate(screenNames.MANGA_DETAIL, { item: item });

export const navigateBack = () => goBack();