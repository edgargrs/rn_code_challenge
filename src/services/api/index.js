import axios from 'axios';
import { baseURL } from 'config';

const api = axios.create({
  baseURL,
  headers: {
    'Cache-Control': 'no-cache',
    'Content-Type': 'application/vnd.api+json',
    'Accept': 'application/vnd.api+json'
  },
  timeout: 15000
});


const createInterceptor = () => {
  api.interceptors.response.use(
    response => response,
    error => {
      let errorResponse = error.response;
      switch (errorResponse.status) {
        case 401:

        break;
        case 406:

        break;
        case 415:

        break;
        case 500:

        break;

        default:
        break;
      }
    }
  );
};

createInterceptor();

const getCategories = () => api.get('categories');

const getAnimesOfCategory = category_id => api.get(`categories/${category_id}/anime`);

const getSearch = (type, search) => api.get(`${type}?filter[text]=${search}`);

export default {
  getCategories,
  getAnimesOfCategory,
  getSearch,
};
