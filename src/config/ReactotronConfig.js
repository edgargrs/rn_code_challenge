import Reactotron, { trackGlobalErrors } from 'reactotron-react-native';
import { reactotronRedux } from 'reactotron-redux';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { reactotronURL } from 'config';

const reactotron = Reactotron.configure({ name: 'rn_code_challenge', host: reactotronURL, port: 9090 })
    .use(reactotronRedux())
    .use(trackGlobalErrors())
    .useReactNative()
    .setAsyncStorageHandler(AsyncStorage)
    .connect();
Reactotron.clear();
console.tron = reactotron;

export default reactotron;