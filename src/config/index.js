import AsyncStorage from '@react-native-async-storage/async-storage';

export const DebugConfig = {
    useFixtures: false,
    yellowBox: false,
    reduxLoggin: __DEV__,
    useReactotron: __DEV__
};

export const REDUX_PERSIST = {
    key: 'rn_code_challenge',
    version: 1.0,
    storage: AsyncStorage,
    blacklist: [],
    whitelist: ['categories', 'favorites']
};

export const baseURL = __DEV__ ? 'https://kitsu.io/api/edge/' : 'https://kitsu.io/api/edge/';
export const reactotronURL = '192.168.100.90';