import { combineReducers } from "redux";

import splash from 'features/splash/reducers';
import categories from 'features/categories/reducers';
import favorites from 'features/favorites/reducers';

export default combineReducers({
  splash,
  categories,
  favorites
});
